import React, { Component } from "react";
import { shoeArr } from "./DataShoe";
import ItemShoe from "./ItemShoe";
import Table_Cart from "./Table_Cart";

export default class Ex_ShoeShop extends Component {
  state = {
    shoeArr: shoeArr,
    cart: [],
  };

  renderShoe = () => {
    return this.state.shoeArr.map((item) => {
      return (
        <ItemShoe addToCart={this.addToCart} shoeData={item} key={item.id} />
      );
    });
  };

  addToCart = (shoe) => {
    let index = this.state.cart.findIndex((item) => {
      return item.id == shoe.id;
    });
    let cloneCart = [...this.state.cart];
    if (index == -1) {
      let newShoe = { ...shoe, quantity: 1 };
      cloneCart.push(newShoe);
    } else {
      cloneCart[index].quantity++;
    }

    this.setState({
      cart: cloneCart,
    });
  };

  removeShoe = (idShoe) => {
    let index = this.state.cart.findIndex((item) => {
      return item.id == idShoe;
    });
    if (index !== -1) {
      let cloneCart = [...this.state.cart];
      cloneCart.splice(index, 1);
      this.setState({
        cart: cloneCart,
      });
    }
  };

  changeQuantity = (idShoe, step) => {
    let index = this.state.cart.findIndex((item) => {
      return item.id == idShoe;
    });

    let cloneCart = [...this.state.cart];

    cloneCart[index].quantity = cloneCart[index].quantity + step;

    if (cloneCart[index].quantity == 0) {
      cloneCart.splice(index, 1);
    }

    this.setState({
      cart: cloneCart,
    });
  };

  render() {
    return (
      <div>
        <div className="title text-danger font-weight-bolder mb-3">
          BÀI TẬP GIỎ HÀNG
        </div>

        {this.state.cart.length > 0 && this.state.cart.length > 0 && (
          <Table_Cart
            changeQuantity={this.changeQuantity}
            removeShoe={this.removeShoe}
            cart={this.state.cart}
          />
        )}

        <div className="row py-2 mx-2">{this.renderShoe()}</div>
      </div>
    );
  }
}
