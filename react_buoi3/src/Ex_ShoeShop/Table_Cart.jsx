import React, { Component } from "react";

export default class Table_Cart extends Component {
  renderTable = () => {
    return this.props.cart.map((item) => {
      return (
        <tr>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.price}</td>
          <td>
            <button
              onClick={() => {
                this.props.changeQuantity(item.id, +1);
              }}
              className="btn btn-success mr-2"
            >
              +
            </button>
            {item.quantity}
            <button
              onClick={() => {
                this.props.changeQuantity(item.id, -1);
              }}
              className="btn btn-danger ml-2"
            >
              -
            </button>
          </td>
          <td>
            <button
              onClick={() => {
                this.props.removeShoe(item.id);
              }}
              className="btn btn-danger"
            >
              Xoá
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div>
        <table className="table">
          <thead>
            <tr>
              <td>Id</td>
              <td>Name</td>
              <td>Price</td>
              <td>Quantity</td>
              <td>Interactive</td>
            </tr>
          </thead>

          <tbody>{this.renderTable()}</tbody>
        </table>
      </div>
    );
  }
}
